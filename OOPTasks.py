__author__ = 'Alex Neshko'
class Date:
    def __init__(self, year, mounth, day):
        self.year = year
        self.mounth = mounth
        self.day = day

    """
    def set_date(self, year, mounth, day):
        self.year = year
        self.mounth = mounth
        self.day = day
    """

    def is_leap_year(self):
        if self.year%4 == 0:
            return True
        else:
            return False

    def is_equal(self, year, mounth, day):
        if (self.year == year) & (self.mounth == mounth) & (self.day == day):
            return True
        else:
            return False

    def __str__(self):
        return "День: " + str(self.day) + " " + "Місяць: " + str(self.mounth) + " " + "Рік: " + str(self.year)

import datetime as dt
class Friend(Date):
    def __init__(self, year, mounth, day, PIB, Phone):
        Date.__init__(self, year, mounth, day)
        self.PIB = PIB
        self.Phone = Phone

    def days_to_next_birthday(self):
        today = dt.date.today()
        birthday_next_year = dt.date(int(2016), int(self.mounth), int(self.day))
        days_left = (birthday_next_year - today)
        return days_left.days

    def __str__(self):
        return "ПІБ: " + str(self.PIB) + " " + "Телефон: " + str(self.Phone) + " " + "День народження: " +  Date.__str__(self)

class Car:
    def __init__(self, name, max_speed):
        self.name = name
        self.max_speed = max_speed

    def cost(self):
        return self.max_speed*100

    def update_model(self):
        self.max_speed+=10
        return self.max_speed

    def info(self):
        return "Назва: " + self.name + ", " + "Максимальна швидкість: " + str(self.max_speed) + \
               ", " + "Вартість: " + str(self.cost())

class Executive_Car(Car):
    def cost(self):
        return self.max_speed*250

    def update_model(self):
        self.max_speed+=5
        return self.max_speed
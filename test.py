__author__ = 'Alex Neshko'
from  OOPTasks import *


date = Date(2016, 4, 14)
print(date)

print(date.is_leap_year())
print(date.is_equal(2016,4,14))
print(date.__str__())

"""
import datetime
now = datetime.datetime.now()
print(now.day)

import datetime as dt
bd = "14/01/2016"

# split the bd string into month, day, year
day, month, year = bd.split("/")
# convert to format datetime.date(year, month, day))
birthday = dt.date(int(year), int(month), int(day))
# get todays date
today = dt.date.today()
# calculate age since birth
age = (today - birthday)
print( "You are %d days old!" % age.days )
print(today+dt.timedelta(days=93))
"""
print("------------------------=================++++++++++++++++++++++++++++")
print("Task 2")
friend = Friend(2001,1,15,"Вася Пупкін", "000-225-252-3")
print(friend.__str__())
print("Днів до наступного дня народження:", friend.days_to_next_birthday())

print("------------------------=================++++++++++++++++++++++++++++")
#help: https://ru.wikipedia.org/wiki/%D0%9E%D0%B1%D1%8A%D0%B5%D0%BA%D1%82%D0%BD%D0%BE-%D0%BE%D1%80%D0%B8%D0%B5%D0%BD%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%BD%D0%BE%D0%B5_%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5_%D0%BD%D0%B0_Python
print("Task 3")
car = Car("Dodge", 140)
print(car.info())
print("Оновимо модель. Тепер максимальна швидкість:", car.update_model())

executive_car = Executive_Car("Zaporoshets", 160)
print(executive_car.info())
print("Оновимо модель. Тепер максимальна швидкість:", executive_car.update_model())

print("well done")